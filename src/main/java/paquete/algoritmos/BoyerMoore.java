package paquete.algoritmos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author FMDD
 */

public class BoyerMoore
{
    // Funcion para encontrar el patron
    public void findPattern(String t, String p)
    {
        char[] text = t.toCharArray();
        char[] pattern = p.toCharArray();
        int pos = indexOf(text, pattern);
        if (pos == -1)
            System.out.println("\nNo se encontró\n");
        else
            System.out.println("Patrón encontrado en la posición: "+ pos);
    }
    // Funcion para hallar la posicion en la que se encontro
    public int indexOf(char[] text, char[] pattern) 
    {
        if (pattern.length == 0) 
            return 0;
        int charTable[] = makeCharTable(pattern);
        int offsetTable[] = makeOffsetTable(pattern);
        for (int i = pattern.length - 1, j; i < text.length;) 
        {
            for (j = pattern.length - 1; pattern[j] == text[i]; --i, --j) 
                     if (j == 0) 
                    return i;
 
              i += Math.max(offsetTable[pattern.length - 1 - j], charTable[text[i]]);
        }
        return -1;
      }
      // Para hacer los saltos en base a los caracteres
    
      private int[] makeCharTable(char[] pattern) 
      {
        final int ALPHABET_SIZE = 256;
        int[] table = new int[ALPHABET_SIZE];
        for (int i = 0; i < table.length; ++i) 
               table[i] = pattern.length;
        for (int i = 0; i < pattern.length - 1; ++i) 
               table[pattern[i]] = pattern.length - 1 - i;
        return table;
      }
      
      private static int[] makeOffsetTable(char[] pattern) 
      {
        int[] table = new int[pattern.length];
        int lastPrefixPosition = pattern.length;
        for (int i = pattern.length - 1; i >= 0; --i) 
        {
            if (isPrefix(pattern, i + 1)) 
                   lastPrefixPosition = i + 1;
              table[pattern.length - 1 - i] = lastPrefixPosition - i + pattern.length - 1;
        }
        for (int i = 0; i < pattern.length - 1; ++i) 
        {
              int slen = suffixLength(pattern, i);
              table[slen] = pattern.length - 1 - i + slen;
        }
        return table;
    }
    // Determina el prefijo del patrón
    private static boolean isPrefix(char[] pattern, int p) 
    {
        for (int i = p, j = 0; i < pattern.length; ++i, ++j) 
            if (pattern[i] != pattern[j]) 
                  return false;
        return true;
    }
    // Determina el sufijo del patron
    private static int suffixLength(char[] pattern, int p) 
    {
        int len = 0;
        for (int i = p, j = pattern.length - 1; i >= 0 && pattern[i] == pattern[j]; --i, --j) 
               len += 1;
        return len;
    }

    public static void main(String[] args) throws IOException
    {    
        long startTime = System.nanoTime();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Patrón Boyer-Moore");
        String text = "Dorothy vivía con sus tíos en una hermosa casa de madera en medio del campo, era una región poco poblada y muy árida. Como único compañero de juego, tenía a Totó, un perrito revoltoso e inteligente. Un día un terrible tornado apareció de la nada y se tragó por completo la casa y el granero. Dorothy y Totó que estaban jugando dentro, se asustaron mucho al notar como la casa de despegaba del suelo. Al asomarse a la ventana y ver aquella enorme casa volando en círculos por todo el terreno no podían creerlo. La casa se mantuvo girando dos o tres veces en el aire, pero luego comenzó a volar en silencio, arrastrada por el viento… Estuvieron así varios días, incrédulos sin poder dejar de mirar por la ventana, hasta que un día, la casa comenzó a subir y subir, hasta el punto en que solo podían ver nubes. Pasaron varios días más, hasta que una mañana, Totó y Dorothy se despertaron con un ruido de madera que crujía. La casa estaba aterrizando sobre un hermoso césped de un verde brillante. Dorothy ya no tenía miedo y, empujada por la curiosidad, comenzó a salir poco a poco para mirar a su alrededor. No había rastro de sus tíos, de la granja, de los demás animales ni de los vecinos… ¿Cómo volverían a casa? ¿Estaban muy lejos? ¿Dónde estaban? Dorothy decidió que había que aventurarse en la espesura del bosque para tratar de encontrar a la forma de vovler a su casa, quizás un leñador les podría indicar el camino. Así que eso hizo, junto a su amigo Totó, comenzó a caminar bosque a través. A penas había recorrido unos metros, cuando en medio del bosque, la niña pudo divisar un extraño camino. Entre los arbustos y el césped cubierto de hojas, aparecían unas grandes baldosas amarillas, de un color parecido al oro, que se colocaban amontonadas, grandes, pequeñas y medianas, cuadradas y redondas, una a una iban conformando un sepentenate camino que se adentraba en el bosque. Sin dudarlo, Dorothy comenzó a caminar sobre las baldosas, dando alegras saltos y canturreando, mientras Totó, algo más prudente, olisqueaba bien ese curioso suelo. Pasaron las horas sin ver a nadie… Cuando a lo lejos, pudieron ver un Espantapájaros que esstaba justo al borde del camino. Se pararon a observarlo un rato y para su sorpresa, el espantapájaros se quito el sombrero y dijo «Buenas tardes» ¡Dorothy casi se cae del susto! mientras que Totó comenzó a ladrar y gruñir. ¿Un espantapájaros que habla? «Perdona si te he asustado ¿tú también vas a ver al Mago de Oz?» – «¿Quién es ese mago?» contestó Dorothy, que aún no podía creer que estaba hablando con un espantapájaros de trapo. «Es el hombre más sabio y poderoso del mundo, todo lo que deseas, él pude encontrarlo. Yo me dirijo a Oz para pedirle un cerebro, estoy cansado de tener una cabeza llena de paja» Entonces, Dorothy supo que si quería encontrar la forma de volver a su casa, aquel mago debía saber la forma de hacerlo. Decidió acompañar al espantapájaros, después de tener que separar a Totó varias veces, ya que en cuento se descuidaba, pe pequeño perro le mordía los tobillos de paja. En el camino se encontraron con el Hombre de Hojalata que estaba sentado en una piedra haciendo caras raras. -«¿Qué te sucede?» El hombre de hojalata, torció el labio y comenzó a hacer unos sonidos extraños que sonaban a lata hueca. «Estoy triste» dijo. Pero había algo raro en su cara. El espantapájaros, demostrando que en vez de cerebro tenía paja, dijo lo primero que pasó por su cabeza: -«No pareces triste, pareces más bien, asustado, feliz, enfadado, alegre, aliviado y cansado… Todo a la vez» «Ese es mi problema» dijo el hombre de hojalata. No tengo sentimientos, necesito un corazón para poder sentir de verdad. Dorothy con su amigo Totó y el espantapájaros invitaron al hombre de hojalata a que les acompañara en busca del Mago de Oz. Así cada uno podría conseguir lo que quería. De repente apareció un león en el camino, todos se asustaron porque no se imaginaban que era el león más cobarde del mundo. Quería ser valiente pero no sabía cómo hacerlo ¡Hasta tenía miedo de su sombra! El león estaba en mitad del camino, caminaba distraído por él, olisqueando el suelo y parándose para lamerse las patas. De pronto, giró la cabeza y se quedó petrificado  al ver a Dorothy, Toto, y sus nuevos amigos totalmente quietos y con cara de miedo. El asustadizo león, pensó que algo terrible debía de haber aparecido de entre los matorrales, y que justo estaba detrás suya, por eso, aquellas personas tenían esa cara de miedo. Como no era novedad, el espantapájaros dijo lo primero que le pasó por la cabeza. -«No nos comas leoncito, no nos comas… Y si quieres comer, que no sea a mi, soy de paja y no tengo buen sabor…» – «¿Comeros yooooo? dijo el león. Si pensaba que había una bestia detrás mía que nos iba a comer a todos. La carcajada fue general, una confusión muy divertida. «Me siento alegre» decía el hombre de hojalata una y otra vez. Pronto hicieron buenas migas con aquel león, que les contó su problema para ser valiente. Así que juntos emprendieron el viaje al lejano reino de Oz para hacer sus peticiones al mago. En dirección al castillo del mago el paisaje se volvió cada vez más extraño y fascinante: curiosas flores y plantas gigantescas sonreían a los recién llegados. En un momento dado, en la cima de una montaña lejana, apareció un enorme castillo: allí vivía el Mago de Oz. Por fin estaban llegando. Sólo tenían que caminar por la larga avenida de baldosas amarillas hasta llegar al castillo y pedirle al mago que cumpliera sus deseos. Cuando llegaron a la puerta, antes de llamar, se prepararon para encontrarse con el Mago de Oz: Dorothy se peinó los rizos y pasó la mano por Totó para peinarle también, el León sacudió el polvo de su melena, el Espantapájaros comprobó que tenía el relleno bien apretado y el Hombre de Hojalata se echó unas gotitas de aceite en las rodillas para no hacer ruido al caminar. Una vez que entraron, encontraron a un anciano con una tierna mirada en su cara. Dorothy le contó toda su historia y después de escuchar sus peticiones el Mago decidió cumplirlas, dándole a cada uno lo que realmente quería… Dorothy soñaba con abrazar a sus tíos de nuevo. El Hombre de Hojalata quería tener un latido en el pecho que le hiciera sentir. El león tener el valor que se espera de él. Y el Espantapájaros quería tener inteligencia y no una cabeza llena de paja. Totó, también cumplió sus deseos y el Mago le concedió un enorme hueso inagotable para morder y relamerse una y otra vez. Todos juntos celebraron un que habían alcanzado su objetivo y pese al largo camino habían conseguido lo que buscaban. La fiesta se alargó hasta muy tarde y Dorothy se quedó dormida, abrazada a Totó. Cuando despertó, estaba en su cama, en su casa y todo estaba en su sitio, sus tíos la esperaban para desayunar… Dorthy se preguntaba si había soñado todo aquello hasta que al irse a calzar, vio que la suela de sus zapatos estaba teñida de amarillo… Fue a buscar a Totó que se encontraba en el jardín mordiendo un hueso gigantesco y para salir de su asombro, notó como unas cuantas briznas de paja se caían de su cabeza. El fin.";
        String pattern = "¿tú también vas a ver al Mago de Oz?";
        BoyerMoore bm = new BoyerMoore(); 
        bm.findPattern(text, pattern);   
        long endTime = System.nanoTime();
        System.out.println("Duración: " + (endTime-startTime)/1e6 + " ms");
    }
}
